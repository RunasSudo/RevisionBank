#   RevisionBank
#   Copyright © 2018  Yingtong Li (RunasSudo)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

import revisionbank.scripts

from datetime import datetime
import flask
import jinja2
import mwparserfromhell as mw
import pytz
import re
import types

class MongoObject:
	@classmethod
	def from_json(cls, json_obj):
		return cls(**json_obj)

class BasePage(MongoObject):
	def __init__(self, name=None):
		self.name = name
	
	@property
	def pretty_name(self):
		return self.name.split('/')[-1]
	
	def to_json(self):
		return {'name': self.name}

class Page404(BasePage):
	@property
	def revisions(self):
		return [Revision404(page=self)]

class Page(BasePage):
	def __init__(self, _id=None, revisions=None, **kwargs):
		super().__init__(**kwargs)
		if revisions is None:
			revisions = []
		self.revisions = revisions
	
	def to_json(self):
		json_obj = super().to_json()
		json_obj.update({'revisions': [r.to_json() for r in self.revisions]})
		return json_obj
	
	@classmethod
	def from_json(cls, json_obj):
		obj = Page(**json_obj)
		obj.revisions = [revision_types[r['type']].from_json(r, obj) for r in obj.revisions]
		return obj

class Revision(MongoObject):
	def __init__(self, page=None, creator=None, creation_date=None, reason=None, type=None):
		self.page = page
		
		self.creator = creator
		self.creation_date = creation_date
		self.reason = reason
	
	def to_json(self):
		return {'creator': self.creator.to_json(), 'creation_date': self.creation_date.strftime('%Y-%m-%dT%H:%M:%SZ'), 'reason': self.reason}
	
	@classmethod
	def from_json(cls, json_obj, page=None):
		import revisionbank.user
		
		obj = cls(**json_obj)
		obj.page = page
		obj.creator = revisionbank.user.GoogleUser.from_json(obj.creator)
		obj.creation_date = pytz.utc.localize(datetime.strptime(json_obj['creation_date'], '%Y-%m-%dT%H:%M:%SZ'))
		return obj

class Revision404(Revision):
	def render_content(self):
		return jinja2.Markup(flask.render_template('page_404.html', page=self.page))
	
	@property
	def content(self):
		return ''

def markup_delimtag(pattern, tag1, tag2, markup):
	markup = markup.replace(jinja2.escape(pattern), jinja2.Markup('\ue000'))
	markup = markup_resub(r'\ue000(.*?)\ue000', r'{0}\1{1}'.format(tag1, tag2), markup)
	markup = markup.replace(jinja2.Markup('\ue000'), jinja2.escape(pattern))
	return markup

def markup_resub(pattern, repl, markup):
	return jinja2.Markup(re.sub(pattern, repl, str(markup)))

class RevisionMarkdown(Revision):
	def __init__(self, content=None, **kwargs):
		super().__init__(**kwargs)
		self.content = content
	
	def render_content(self):
		paragraphs = self.content.split('\n\n')
		
		markup = jinja2.Markup()
		for paragraph in paragraphs:
			wikitext = mw.parse(paragraph)
			nodes = wikitext.filter(recursive=False)
			
			# If contains text, then this is a paragraph
			is_text = any(isinstance(node, mw.nodes.text.Text) for node in nodes)
			if is_text:
				markup += jinja2.Markup('<p>')
			
			# Walk and render nodes
			for node in nodes:
				nmarkup = jinja2.Markup()
				
				if isinstance(node, mw.nodes.text.Text):
					nmarkup = jinja2.escape(node.value)
					nmarkup = markup_delimtag('***', '<b><i>', '</i></b>', nmarkup)
					nmarkup = markup_delimtag('**', '<b>', '</b>', nmarkup)
					nmarkup = markup_delimtag('*', '<i>', '</i>', nmarkup)
				elif isinstance(node, mw.nodes.template.Template):
					pagescript_json = flask.current_app.mongo.db.pages.find_one({'name': 'Script:' + str(node.name)})
					
					if pagescript_json is None:
						if str(node.name) in revisionbank.scripts.builtin_scripts:
							script = revisionbank.scripts.builtin_scripts[str(node.name)]
							nmarkup = script.render(self, node)
						else:
							nmarkup = jinja2.Markup('<b>Unknown script {}</b>').format(str(node.name))
					else:
						pagescript = Page.from_json(pagescript_json)
						revision = pagescript.revisions[-1]
						script = revision.script
						nmarkup = script.render(self, node)
				
				markup += nmarkup
			
			if is_text:
				markup += jinja2.Markup('</p>')
		
		return markup
	
	def to_json(self):
		json_obj = super().to_json()
		json_obj.update({'type': 'markdown', 'content': self.content})
		return json_obj

class RevisionScript(Revision):
	def __init__(self, content=None, **kwargs):
		super().__init__(**kwargs)
		self.content = content
	
	@property
	def script(self):
		context_dict = {}
		exec(self.content, {}, context_dict)
		context = type(self.page.name.split('/')[-1], (revisionbank.scripts.Script,), context_dict)
		return context
	
	def render_content(self):
		import pygments, pygments.lexers, pygments.formatters
		return jinja2.Markup(pygments.highlight(self.content, pygments.lexers.PythonLexer(), pygments.formatters.HtmlFormatter()) + '<style type="text/css">' + pygments.formatters.HtmlFormatter().get_style_defs('.highlight') + '</style>')
	
	def to_json(self):
		json_obj = super().to_json()
		json_obj.update({'type': 'script', 'content': self.content})
		return json_obj

revision_types = {
	'markdown': RevisionMarkdown,
	'script': RevisionScript
}
